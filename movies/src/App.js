import './App.css';
import Login from './screens/Login';
import { Fragment } from 'react';
import { useSelector } from 'react-redux';
import Dashboard from './screens/Dashboard';
import { Route, Routes } from "react-router-dom";
import Detail from './screens/Detail';
import ShowDetail from './screens/ShowDetail';
import SearchResult from './screens/SearchResults';
import RecommendRandom from './screens/RecommendRandom';

function App() {
  const isAuth = useSelector((state) => {
    return state.user.isAuth;
  })

  return (
    <Fragment>
      {isAuth ? (
        <>
          <Routes>
            <Route path="/" element={<Dashboard/>} />
            <Route path="/movie/detail" element={<Detail/>}/>
            <Route path="/show/detail" element={<ShowDetail/>}/>
            <Route path="/search" element={<SearchResult />} />
            <Route path="/random" element={<RecommendRandom />} />
          </Routes>
        </>
      ) : (
        <Login/>
      )}
    </Fragment>
  );
}

export default App;
