import NavBar from "./NavBar";
import { Row, Col } from 'react-bootstrap';
import Rating from 'react-rating';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'
import '../screens/Dashboard.css'
import 'react-loading-skeleton/dist/skeleton.css';

const DetailSkeleton = () => {
    return(
        <div>
            <NavBar/>
            <br/>
            <Row className="mr-auto horizontal-margin">
                <Col sm={4}>
                    <Skeleton className="custom-image-detail-size mx-auto d-block" width='286px' height='429px'/>
                </Col>
                <Col sm={8}>
                    <Row className="horizontal-margin">
                        <Col className="detail-col-custom-size">
                            <p className="dasboard-sub-title">Movie Title</p>
                            <p><Skeleton/></p>
                        </Col>
                    </Row>
                    <Row className="horizontal-margin">
                        <Col className="detail-col-custom-size">
                            <p className="dasboard-sub-title">Genres</p>
                            <Skeleton/>
                        </Col>
                    </Row>
                    <Row className="horizontal-margin">
                        <Col className="detail-col-custom-size">
                            <p className="dasboard-sub-title">Budget</p>
                            <Skeleton/>
                        </Col>
                    </Row>
                    <Row className="horizontal-margin">
                        <Col className="detail-col-custom-size">
                            <p className="dasboard-sub-title">Average Rating</p>
                            <Skeleton width='300px' height='30px'/>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="horizontal-margin margin-custom-bottom-row">
                <Col md={{ span: 10, offset: 1 }} className="allignment-custom-bottom-row">
                    <p className="dasboard-sub-title">Synopsis</p>
                    <Skeleton count={3}/>
                </Col>
            </Row>
            <Row className="horizontal-margin margin-custom-bottom-row ">
                <Col md={{ span: 10, offset: 1 }} className="allignment-custom-bottom-row">
                    <p className="dasboard-sub-title">Trailer</p>
                    <Skeleton width='420px' height='300px'/>
                </Col>
            </Row>
        </div>
    );
}

export default DetailSkeleton;