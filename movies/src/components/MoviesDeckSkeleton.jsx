import { Card, Row, Col, Container } from 'react-bootstrap';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';
import '../screens/Dashboard.css'

const MoviesDeckSkeleton = () => {
    return(
        <div>
            <Container fluid className="App py-2 overflow-hidden">
            <Row className="flex-box-title-custom">
                <Skeleton height='44px' width='500px' />
            </Row>
            <Row className="card-example d-flex flex-row flex-nowrap overflow-auto deck-scaling justify-content-md-center">
                    {Array.from({ length: 4 }).map((_, idx) => {
                        return(
                            <Card className="custom-dashboard-card" >
                                <Card.Body>
                                    <Row>
                                        <Col className="custom-dashboard-col-height">
                                            <Card.Title className=""><Skeleton/></Card.Title>
                                        </Col>
                                    </Row>
                                    <Skeleton width='230px' height='341px'/>
                                    <Skeleton className="dashboard-custom-button-detail" width='67px' height='38px'/>
                                    <footer>
                                    <small className="text-muted"><Skeleton width='68px' height='19px'/></small>
                                    </footer>
                                </Card.Body>
                            </Card>
                        );
                    })}
                </Row>
            </Container>
        </div>
    );
}

export default MoviesDeckSkeleton;