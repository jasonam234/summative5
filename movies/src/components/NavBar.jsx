import * as React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import './NavBar.css';
import { useDispatch } from 'react-redux';
import { userActions } from '../store/user';
import { useNavigate } from 'react-router';
import { useState } from 'react';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button';

const NavBar = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    let [search, setSearch] = useState({
        search:""
    });

    function logoutHandler(){
        dispatch(userActions.logout());
        navigate({pathname: "/"});
    }

    function homeHandler(){
        navigate({pathname: "/"});
    }

    function searchHandler(events){
        console.log(events.target.value);
        setSearch({
            ...search, search: events.target.value
        });
    }

    function buttonHandler(query){
        dispatch(userActions.search(query));
        navigate({pathname: "/search"})
    }

    return (
        <Navbar expand="lg" className="navbar-background">
            <Container>
                <Navbar.Brand onClick={homeHandler}>Recommndr</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    <Nav.Link onClick={homeHandler}>Home</Nav.Link>
                    <Form className="d-flex">
                        <FormControl
                        type="search"
                        placeholder="Search"
                        className="me-2"
                        aria-label="Search"
                        onChange={searchHandler}
                        />
                        <Button onClick={() => buttonHandler(search.search)} variant="primary">Search</Button>
                    </Form>
                </Nav>
                <Nav className="ml-auto">
                    <Nav.Link onClick={logoutHandler}>Logout</Nav.Link>
                </Nav>
                </Navbar.Collapse>

            </Container>
        </Navbar>
    );
}

export default NavBar;