import { useState, useEffect } from 'react';
import NavBar from '../components/NavBar'
import './Dashboard.css'
import { useDispatch, useSelector } from 'react-redux';
import { moviesActions } from '../store/movies';
import { useNavigate } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import MoviesDeck from './MoviesDeck';
import ShowDeck from './ShowsDeck';

const Dashboard = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    function detailHandler(movieId){
        dispatch(moviesActions.getMovieId(movieId));
        navigate({pathname: "/detail"});
    }

    function buttonHandler(){
        navigate({pathname: "/random"});
    }

    return(
        <div>
            <NavBar/>

            <Button onClick={buttonHandler} className="recommend-button" size="lg">Recommend Something</Button>
            <MoviesDeck/>
            <br />
            <ShowDeck/>
        </div>
    );
}

export default Dashboard;