import NavBar from "../components/NavBar";
import { useDispatch, useSelector } from 'react-redux';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { moviesActions } from '../store/movies';
import { Row, Col } from 'react-bootstrap';
import Rating from 'react-rating';
import DetailSkeleton from '../components/DetailSkeleton';
import './Detail.css';
import MovieTrailer from "./MovieTrailer";

const Detail = () => {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [genres, setGenres] = useState([]);

    const movieId = useSelector((state) => {
        return state.movies.movieId;
    });

    const movieData = useSelector((state) => {
        return state.movies.movieIdData;
    });

    useEffect(() => {
        setIsLoading(true);
        const apiEndpoint = "https://api.themoviedb.org/3/movie/" + movieId + "?api_key=" + process.env.REACT_APP_NOT_SECRET_CODE + "&language=en-US";
        axios.get(apiEndpoint)
        .then(res => {
            dispatch(moviesActions.getMovieIdData(res.data));
            setGenres(res.data.genres);
            setTimeout(() => { setIsLoading(false); }, 2000);
        })
        
    }, [dispatch])

    const posterLink = "https://image.tmdb.org/t/p/w500/" + movieData.poster_path;
    const budget = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD', currencyDisplay: 'narrowSymbol'}).format(movieData.budget);
    

    if(isLoading === true){
        return(
            <DetailSkeleton/>
        );
    }

    return(
        <div>
            <NavBar/>
            <br/>
            <Row className="mr-auto horizontal-margin">
                <Col sm={4}>
                    <img className="custom-image-detail-size mx-auto d-block" src={posterLink} alt="movie poster" />
                </Col>
                <Col sm={8}>
                    <Row className="horizontal-margin">
                        <Col className="detail-col-custom-size">
                            <p className="dasboard-sub-title">Movie Title</p>
                            <p>{movieData.original_title}</p>
                        </Col>
                    </Row>
                    <Row className="horizontal-margin">
                        <Col className="detail-col-custom-size">
                            <p className="dasboard-sub-title">Genres</p>
                            <div>
                                {genres.map((values, index) => {
                                    return(
                                        <p className="custom-genre-paragraph">{values.name}&nbsp;</p>
                                    );
                                })}
                            </div>
                            <br/>
                        </Col>
                    </Row>
                    <Row className="horizontal-margin">
                        <Col className="detail-col-custom-size">
                            <p className="dasboard-sub-title">Budget</p>
                            <p>{budget}</p>
                        </Col>
                    </Row>
                    <Row className="horizontal-margin">
                        <Col className="detail-col-custom-size">
                            <p className="dasboard-sub-title">Average Rating</p>
                            <p><Rating initialRating={movieData.vote_average} stop={10} readonly/></p>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="horizontal-margin margin-custom-bottom-row">
                <Col md={{ span: 10, offset: 1 }} className="allignment-custom-bottom-row">
                    <p className="dasboard-sub-title">Synopsis</p>
                    <p>{movieData.overview}</p>
                </Col>
            </Row>
            <MovieTrailer/>
        </div>
    );
    


}

export default Detail;