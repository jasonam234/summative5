import { useState } from 'react';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form'
import { useDispatch } from 'react-redux';
import './FormLogin.css'
import { userActions } from '../store/user';

const FormLogin = () => {
    const dispatch = useDispatch();
    let [data, setData] = useState({
        username:"",
        password:""
    });

    function usernameHandler(events){
        console.log(events.target.value)
        setData({
            ...data, username: events.target.value
        });
    }

    function passwordHandler(events){
        console.log(events.target.value)
        setData({
            ...data, password: events.target.value
        });
    }

    function loginHandler(events){
        events.preventDefault();
        dispatch(userActions.login(data));
    }

    return(
        <Container>
            <Row>
                <Col>
                    <div>
                        <p className="color-text">Username</p>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Form>
                        <Form.Group className="mb-3" onChange={ usernameHandler } controlId="exampleForm.ControlInput1">
                            <Form.Control type="text" placeholder="username" />
                        </Form.Group>
                    </Form>
                </Col>
            </Row>
            <Row>
                <Col>
                    <div>
                        <p className="color-text">Password</p>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Form>
                        <Form.Group className="mb-3" onChange={ passwordHandler } controlId="exampleForm.ControlInput2">
                            <Form.Control type="password" placeholder="password" />
                        </Form.Group>
                    </Form>
                </Col>
            </Row>
            <button className="custom-button" onClick={loginHandler}>Login</button>
        </Container>
    );
}

export default FormLogin;