
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import FormLogin from './FormLogin';
import './Login.css';

const Login = () => {
    return(
        <div>
            <Container>
                <Row>
                    <Col>
                        <div className="cards">
                            <Card>
                                <center>
                                    <p className="title">Login</p>
                                    <hr/>
                                </center>
                                <Card.Body>
                                    <FormLogin />
                                </Card.Body>
                            </Card>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default Login;