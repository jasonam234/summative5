import NavBar from "../components/NavBar";
import { useDispatch, useSelector } from 'react-redux';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { moviesActions } from '../store/movies';
import { Row, Col } from 'react-bootstrap';
import './Detail.css';

const MovieTrailer = () => {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    let trailerLink = "";

    const movieId = useSelector((state) => {
        return state.movies.movieId;
    });

    const movieVideo = useSelector((state) => {
        return state.movies.movieTrailers;
    })

    useEffect(() => {
        setIsLoading(true);
        const apiVideoEndpoint = "https://api.themoviedb.org/3/movie/"+ movieId +"/videos?api_key=" + process.env.REACT_APP_NOT_SECRET_CODE + "&language=en-US";
        axios.get(apiVideoEndpoint)
        .then(res => {
            console.log(res.data)
            dispatch(moviesActions.getMovieVideo(res.data));
            setIsLoading(false);
        })
        
    }, [dispatch])

    console.log(movieVideo);

    if(movieVideo.length === 0){
        trailerLink = "";
    }else{
        trailerLink = "https://www.youtube.com/embed/" + movieVideo.results[0].key + "?autoplay=1&mute=1";
    }
    

    return(
        <Row className="horizontal-margin margin-custom-bottom-row ">
            <Col md={{ span: 10, offset: 1 }} className="allignment-custom-bottom-row">
                <p className="dasboard-sub-title">Trailer</p>
                <iframe width="420" height="300" src={ trailerLink }/>
            </Col>
        </Row>
    );

}

export default MovieTrailer;