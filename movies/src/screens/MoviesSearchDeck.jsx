import { useState, useEffect } from 'react';
import axios from 'axios';
import './Dashboard.css'
import { useDispatch, useSelector } from 'react-redux';
import { moviesActions } from '../store/movies';
import { Card, Row, Col, Container } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import MoviesDeckSkeleton from '../components/MoviesDeckSkeleton';

const MoviesSearchDeck = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const query = useSelector((state) => {
        return state.user.search;
    });

    const [isLoading, setIsLoading] = useState(false);
    const listPlayingMovies = useSelector((state) => state.movies.listMovies);

    useEffect(() => {
        setIsLoading(true);
        axios.get("https://api.themoviedb.org/3/search/movie?api_key=" + process.env.REACT_APP_NOT_SECRET_CODE + "&language=en-US&query=" + query + "&page=1&include_adult=false")
        .then(res => {
            dispatch(moviesActions.getAllPlayingMovies(res.data.results));
            setIsLoading(false);
        })

        
    }, [dispatch, query])

    if(isLoading === true){
        return <MoviesDeckSkeleton/>
    }

    if(listPlayingMovies.length === 0){
        return <p></p>
    }

    function detailHandler(movieId){
        dispatch(moviesActions.getMovieId(movieId));
        navigate({pathname: "/movie/detail"});
    }
    return(
        <Container fluid className="App py-2 overflow-hidden">
                <Row className="flex-box-title-custom">
                   Movies with {query} words
                </Row>
                <Row className="card-example d-flex flex-row flex-nowrap overflow-auto deck-scaling">
                    {listPlayingMovies.map((value, index) => {
                        let posterLink = "https://image.tmdb.org/t/p/w500/" + value.poster_path;
                        return(
                                <Card key={index} className="custom-dashboard-card" >
                                    <Card.Body>
                                        <Row>
                                            <Col className="custom-dashboard-col-height">
                                                <Card.Title className="">{value.original_title}</Card.Title>
                                            </Col>
                                        </Row>
                                        <Row className="deck-row-image">
                                            <img onClick={() => detailHandler(value.id)} src={ posterLink }/>
                                        </Row>
                                        <Button className="dashboard-custom-button-detail" onClick={() => detailHandler(value.id)}>Detail</Button>
                                        <footer>
                                        <small className="text-muted">Rating : {value.vote_average}</small>
                                        </footer>
                                    </Card.Body>
                                </Card>
                        );
                    })}
                </Row>
            </Container>    
    );
}

export default MoviesSearchDeck;