import NavBar from "../components/NavBar";
import { useDispatch, useSelector } from 'react-redux';
import MoviesSearchDeck from "./MoviesSearchDeck";
import ShowsSearchDeck from "./ShowsSearchDeck";
import './SearchResult.css';

const SearchResult = () => {
    const movies = useSelector((state) => {
        return state.movies.listMovies;
    });

    const shows = useSelector((state) => {
        return state.shows.listShows;
    });

    console.log(movies);
    console.log(shows);

    if(movies.length === 0 && shows.length === 0){
        return(
            <div>
                <NavBar/>
                <h1 className="center-response">Movie or Show Not Found</h1>
                
                <MoviesSearchDeck/>
                <br />
                <ShowsSearchDeck/>
            </div>
        );
    }

    return(
        <div>
            <NavBar/>
            <MoviesSearchDeck/>
            <br />
            <ShowsSearchDeck/>
        </div>
    );
}

export default SearchResult;