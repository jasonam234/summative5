import NavBar from "../components/NavBar";
import { useDispatch, useSelector } from 'react-redux';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { moviesActions } from '../store/movies';
import { Row, Col } from 'react-bootstrap';
import Rating from 'react-rating';
import ShowDetailSkeleton from '../components/ShowDetailSkeleton';
import './Detail.css';
import { showsAction } from "../store/shows";
import ShowsTrailer from "./ShowsTrailer";

const ShowDetail = () => {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [genres, setGenres] = useState([]);

    const showId = useSelector((state) => {
        return state.shows.showId;
    })

    const showData = useSelector((state) => {
        return state.shows.showIdData;
    })

    const posterLink = "https://image.tmdb.org/t/p/w500/" + showData.poster_path;

    useEffect(() => {
        setIsLoading(true);
        const apiEndpoint = "https://api.themoviedb.org/3/tv/"+ showId +"?api_key=" + process.env.REACT_APP_NOT_SECRET_CODE;
        axios.get(apiEndpoint)
        .then(res => {
            dispatch(showsAction.getShowIdData(res.data));
            setGenres(res.data.genres);
            setTimeout(() => { setIsLoading(false); }, 2000);
        })
        
    }, [dispatch])

    console.log(showData)
    if(isLoading === true){
        return <ShowDetailSkeleton />
    }

    return(
        <div>
            <NavBar/>
            <br/>
            <Row className="mr-auto horizontal-margin">
                <Col sm={4}>
                    <img className="custom-image-detail-size mx-auto d-block" src={posterLink} alt="movie poster" />
                </Col>
                <Col sm={8}>
                    <Row className="horizontal-margin">
                        <Col className="detail-col-custom-size">
                            <p className="dasboard-sub-title">Show Title</p>
                            <p>{showData.name}</p>
                        </Col>
                    </Row>
                    <Row className="horizontal-margin">
                        <Col className="detail-col-custom-size">
                            <p className="dasboard-sub-title">Genres</p>
                            <div>
                                {genres.map((values, index) => {
                                    return(
                                        <p className="custom-genre-paragraph">{values.name}&nbsp;</p>
                                    );
                                })}
                            </div>
                            <br/>
                        </Col>
                    </Row>
                    <Row className="horizontal-margin">
                        <Col className="detail-col-custom-size">
                            <p className="dasboard-sub-title">Release Date</p>
                            <p>{showData.first_air_date}</p>
                        </Col>
                    </Row>
                    <Row className="horizontal-margin">
                        <Col className="detail-col-custom-size">
                            <p className="dasboard-sub-title">Average Rating</p>
                            <p><Rating initialRating={showData.vote_average} stop={10} readonly/></p>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="horizontal-margin margin-custom-bottom-row">
                <Col md={{ span: 10, offset: 1 }} className="allignment-custom-bottom-row">
                    <p className="dasboard-sub-title">Synopsis</p>
                    <p>{showData.overview}</p>
                </Col>
            </Row>
            <ShowsTrailer/>
        </div>
    );
}

export default ShowDetail;