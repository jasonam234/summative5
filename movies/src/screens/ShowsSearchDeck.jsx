import { useState, useEffect } from 'react';
import axios from 'axios';
import './Dashboard.css'
import { useDispatch, useSelector } from 'react-redux';
// import { showsActions } from '../store/shows';
import { Card, Row, Col, Container } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import { showsAction } from '../store/shows';
import MoviesDeckSkeleton from '../components/MoviesDeckSkeleton';
import { ContactsOutlined } from '@mui/icons-material';
import ShowsTrailer from './ShowsTrailer';

const ShowsSearchDeck = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const [isLoading, setIsLoading] = useState(false);
    const listPopularShows = useSelector((state) => state.shows.listShows);

    const query = useSelector((state) => {
        return state.user.search;
    });
    
    console.log(query)

    useEffect(() => {
        setIsLoading(true);
        console.log("here")
        axios.get("https://api.themoviedb.org/3/search/tv?api_key=" + process.env.REACT_APP_NOT_SECRET_CODE 
            + "&language=en-US&page=1&query=" + query + "&include_adult=false")
        .then(res => {
            dispatch(showsAction.getAllPopularShow(res.data.results));
            setIsLoading(false);
        })
    }, [dispatch, query])

    if(isLoading === true){
        return <MoviesDeckSkeleton/>
    }

    function detailHandler(showId){
        dispatch(showsAction.getShowId(showId))
        navigate({pathname: "/show/detail"});
    }

    console.log(listPopularShows.length);

    if(listPopularShows.length === 0){
        return <p></p>
    }

    return(
        <Container fluid className="App py-2 overflow-hidden">
                <Row className="flex-box-title-custom">
                   TV shows with {query} word
                </Row>
                <Row className="card-example d-flex flex-row flex-nowrap overflow-auto deck-scaling">
                {listPopularShows.map((value, index) => {
                        let posterLink = "https://image.tmdb.org/t/p/w500/" + value.poster_path;
                        return(
                            <Card key={index} className="custom-dashboard-card" >
                                <Card.Body>
                                    <Row>
                                        <Col className="custom-dashboard-col-height">
                                            <Card.Title className="">{value.name}</Card.Title>
                                        </Col>
                                    </Row>
                                    <Row className="deck-row-image">
                                        <img onClick={() => detailHandler(value.id)} src={ posterLink }/>
                                    </Row>
                                    <Button className="dashboard-custom-button-detail" onClick={() => detailHandler(value.id)}>Detail</Button>
                                    <footer>
                                    <small className="text-muted">Rating : {value.vote_average}</small>
                                    </footer>
                                </Card.Body>
                            </Card>
                        );
                    })}
                </Row>
            </Container>    
    );
}

export default ShowsSearchDeck;