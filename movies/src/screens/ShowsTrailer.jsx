import NavBar from "../components/NavBar";
import { useDispatch, useSelector } from 'react-redux';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { moviesActions } from '../store/movies';
import { Row, Col } from 'react-bootstrap';
import Rating from 'react-rating';
import ShowDetailSkeleton from '../components/ShowDetailSkeleton';
import './Detail.css';
import { showsAction } from "../store/shows";

const ShowsTrailer = () => {
    const dispatch = useDispatch();
    let trailerLink = "";
    const [isLoading, setIsLoading] = useState(false);
    const [genres, setGenres] = useState([]);

    const showId = useSelector((state) => {
        return state.shows.showId;
    })

    const showVideo = useSelector((state) => {
        return state.shows.showsTrailers;
    })

    useEffect(() => {
        setIsLoading(true);
        const apiEndpoint = "https://api.themoviedb.org/3/tv/" + showId + "/videos?api_key=" + process.env.REACT_APP_NOT_SECRET_CODE 
            + "&language=en-US";
        axios.get(apiEndpoint)
        .then(res => {
            dispatch(showsAction.getShowVideo(res.data));
        })
        
    }, [dispatch])

    console.log(showVideo)

    if(showVideo.length === 0){
        trailerLink = "";
    }else{
        trailerLink = "https://www.youtube.com/embed/" + showVideo.results[0].key + "?autoplay=1&mute=1";
    }
    
    return(
        <Row className="horizontal-margin margin-custom-bottom-row ">
            <Col md={{ span: 10, offset: 1 }} className="allignment-custom-bottom-row">
                <p className="dasboard-sub-title">Trailer</p>
                <iframe width="420" height="300" src={ trailerLink }/>
            </Col>
        </Row>
    );
}

export default ShowsTrailer;