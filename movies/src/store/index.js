import { configureStore } from "@reduxjs/toolkit";
import user from './user';
import movies from './movies';
import shows from './shows';

const store = configureStore({
    reducer: {
        user: user,
        movies: movies,
        shows: shows,
    }
});

export default store;