import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    listMovies: [],
    movieId: 0,
    movieIdData: {},
    movieTrailers: []
}

const moviesSlices = createSlice({
    name: "movies",
    initialState: initialState,
    reducers: {
        getAllPlayingMovies(state,data){
            state.listMovies = data.payload;
        },
        getMovieId(state, data){
            state.movieId = data.payload;
        },
        getMovieIdData(state,data){
            state.movieIdData = data.payload;
        },
        getMovieVideo(state, data){
            state.movieTrailers = data.payload;
            console.log(state.movieTrailers);
        }
    },
});

export const moviesActions = moviesSlices.actions;

export default moviesSlices.reducer;