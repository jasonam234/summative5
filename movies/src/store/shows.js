import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    listShows: [],
    showId: 0,
    showIdData: {},
    showsTrailers: [],
}

const showsSlices = createSlice({
    name: "shows",
    initialState: initialState,
    reducers: {
        getAllPopularShow(state,data){
            state.listShows = data.payload;
        },
        getShowId(state, data){
            state.showId = data.payload;
        },
        getShowIdData(state,data){
            state.showIdData = data.payload;
        },        
        getShowVideo(state, data){
            state.showsTrailers = data.payload;
        }
    },
});

export const showsAction = showsSlices.actions;

export default showsSlices.reducer;