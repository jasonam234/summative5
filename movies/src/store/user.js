import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isAuth: false,
    isError: null,
    search: ""
};

const userSlices = createSlice({
    name: "user",
    initialState: initialState,
    reducers: {
        login(state, data){
            console.log("login here");
            if(data.payload.username === "aaa"){
                state.isAuth = true;
            }else{
                alert("wrong username");
            }
        },
        logout(state){
            state.isAuth = false;
        },
        search(state, data){
            state.search = data.payload;
            console.log(data.payload);
        }
    }
});

export const userActions = userSlices.actions;
export default userSlices.reducer;